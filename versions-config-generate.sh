#!/usr/bin/env sh

set -e

VERSIONS_FILE=versions.txt
TEMPLATE_FILE=versions-config-template.yml
TARGET_FILE=${1}

cp ${TEMPLATE_FILE} ${TARGET_FILE}

while read template image; do
    cat >> ${TARGET_FILE} <<EOF
test ${image}:
  image: ${image}
  extends: .${template}

EOF
done < ${VERSIONS_FILE}

